$(document).foundation();
dateA = new Date();
time_zoneA = dateA.getTimezoneOffset()/(-60);

//Enter the code for the first countdown here. Make sure the bit inside the $('') says #countdown-1
$("#countdown-1").ResponsiveCountdown({
  target_date:"2019/5/30 11:59:59",
  time_zone:time_zoneA,
  target_future:true,
  set_id:0,pan_id:1,
  day_digits:2,
  fillStyleSymbol1:"rgba(255, 255, 157, 1)",
  fillStyleSymbol2:"rgba(255, 255, 157, 1)",
  fillStylesPanel_g1_1:"rgba(77, 60, 44, 1)",
  fillStylesPanel_g1_2:"rgba(30,30,30,1)",
  fillStylesPanel_g2_1:"rgba(77, 60, 44, 1)",
  fillStylesPanel_g2_2:"rgba(30, 30, 30, 1)",
  text_color:"rgba(255,255,255,1)",
  text_glow:"rgba(0,0,0,1)",
  show_ss:false,show_mm:true,
  show_hh:true,show_dd:true,
  f_family:"veranda",show_labels:true,
  type3d:"single",max_height:300,
  days_long:"",days_short:"",
  hours_long:"",hours_short:"hh",
  mins_long:"",mins_short:"",
  secs_long:"",secs_short:"ss",
  min_f_size:9,max_f_size:30,
  spacer:"none",groups_spacing:5,text_blur:2,
  font_to_digit_ratio:0.125,labels_space:1.2,
  complete: function(){
    pause_anim();
  }
});


//this is important to use
target_date = new Date();//this is now date/time
target_date = new Date(target_date.getTime() + 1*30*1000 + 1000);//this is now + 30 min * 60 sec * 1000 ms. i add 1 sec more cause i have glitches in this mode.
time_zone = target_date.getTimezoneOffset()/(-60);//this gets visitor's timezone
target_date = target_date.toDateString()+' '+target_date.toTimeString();


//Enter the code for the second countdown here. Make sure the bit inside the $('') says #countdown-2
$("#countdown-2").ResponsiveCountdown({
  target_date:target_date,
  time_zone:time_zone,
  target_future:true,
  set_id:0,pan_id:1,day_digits:2,
  fillStyleSymbol1:"rgba(255, 255, 157, 1)",
  fillStyleSymbol2:"rgba(255, 255, 157, 1)",
  fillStylesPanel_g1_1:"rgba(77, 60, 44, 1)",
  fillStylesPanel_g1_2:"rgba(30,30,30,1)",
  fillStylesPanel_g2_1:"rgba(77, 60, 44, 1)",
  fillStylesPanel_g2_2:"rgba(30, 30, 30, 1)",
  text_color:"rgba(255,255,255,1)",
  text_glow:"rgba(0,0,0,1)",
  show_ss:true,show_mm:true,
  show_hh:true,show_dd:false,
  f_family:"veranda",show_labels:true,
  type3d:"single",max_height:300,
  days_long:"",days_short:"",
  hours_long:"",hours_short:"hh",
  mins_long:"",mins_short:"",
  secs_long:"",secs_short:"ss",
  min_f_size:9,max_f_size:30,
  spacer:"none",groups_spacing:5,text_blur:2,
  font_to_digit_ratio:0.125,labels_space:1.2,
  complete: function(){
    pause_anim();
  }
});

//this is important to use
target_date2 = new Date();//this is now date/time
target_date2 = new Date(target_date2.getTime() + 2*60*1000 + 1000);//this is now + 30 min * 60 sec * 1000 ms. i add 1 sec more cause i have glitches in this mode.
time_zone2 = target_date2.getTimezoneOffset()/(-60);//this gets visitor's timezone
target_date2 = target_date2.toDateString()+' '+target_date2.toTimeString();


//Enter the code for the third countdown here. Make sure the bit inside the $('') says #countdown-3
$("#countdown-3").ResponsiveCountdown({
  target_date:target_date2,
  time_zone:time_zone2,
  target_future:true,
  set_id:0,pan_id:1,day_digits:2,
  fillStyleSymbol1:"rgba(255, 255, 157, 1)",
  fillStyleSymbol2:"rgba(255, 255, 157, 1)",
  fillStylesPanel_g1_1:"rgba(77, 60, 44, 1)",
  fillStylesPanel_g1_2:"rgba(30,30,30,1)",
  fillStylesPanel_g2_1:"rgba(77, 60, 44, 1)",
  fillStylesPanel_g2_2:"rgba(30, 30, 30, 1)",
  text_color:"rgba(255,255,255,1)",
  text_glow:"rgba(0,0,0,1)",
  show_ss:true,show_mm:true,
  show_hh:true,show_dd:false,
  f_family:"veranda",show_labels:true,
  type3d:"single",max_height:300,
  days_long:"",days_short:"",
  hours_long:"",hours_short:"hh",
  mins_long:"",mins_short:"",
  secs_long:"",secs_short:"ss",
  min_f_size:9,max_f_size:30,
  spacer:"none",groups_spacing:5,text_blur:2,
  font_to_digit_ratio:0.125,labels_space:1.2,
  complete: function(){
    pause_anim();
  }
});